import { Component, Output, EventEmitter } from '@angular/core';
import { Person } from './model/person';

@Component({
    selector: 'user-form',
    templateUrl: 'app7/form.component.html',
})
export class FormComponent {
    
    @Output('updatePerson')
    userUpdated = new EventEmitter();

    newPerson:Person;

    constructor() {
        this.newPerson = new Person("","");
    }

    saveName(event:Person):void {
        console.log(event);
        this.userUpdated.emit(this.newPerson);

        this.newPerson = new Person("","");
    }
}
