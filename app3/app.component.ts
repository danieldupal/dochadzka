import { Component } from '@angular/core';

@Component({
    selector: 'my-app',
    templateUrl: 'app3/app.component.html',
    styleUrls: ['app3/css/style.css']
})
export class AppComponent {
    
    title:string;
    footer:string;

    name:string;

    constructor() {
        this.title = "Unicorn dochadzka";
        this.footer = "All rights reserved";
    }
}
