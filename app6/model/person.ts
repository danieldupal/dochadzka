export class Person {
    firstName: string;
    lastName: string;
    isic?: string;

    constructor(firstName:string, lastName:string, isic?:string) {
        this.firstName = firstName;
        this.lastName = lastName;

        this.isic = isic ? isic : "-";
    }
}