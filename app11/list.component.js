"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@angular/core");
const router_1 = require("@angular/router");
const user_service_1 = require("./service/user.service");
let ListComponent = class ListComponent {
    constructor(userService, router) {
        this.userService = userService;
        this.router = router;
    }
    ngOnInit() {
        this.userService.list().subscribe((users) => {
            this.userList = users;
        });
    }
    edit(index) {
        this.router.navigate(['user/' + index]);
    }
};
ListComponent = __decorate([
    core_1.Component({
        selector: 'user-list',
        template: `
        <md-list>
          <md-list-item *ngFor="let user of userList; let i = index" [attr.data-index]="i">
            <h3 md-line> {{user.firstName}} {{user.lastName}}</h3>
            <p md-line> {{user.isic ? user.isic : "-"}} </p>
            <button (click)="edit(i)" md-mini-fab><md-icon>edit</md-icon></button>
          </md-list-item>
        </md-list>
        
    `
    }),
    __metadata("design:paramtypes", [user_service_1.UserService,
        router_1.Router])
], ListComponent);
exports.ListComponent = ListComponent;
//# sourceMappingURL=list.component.js.map