"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@angular/core");
const router_1 = require("@angular/router");
const person_1 = require("./model/person");
const user_service_1 = require("./service/user.service");
let FormComponent = class FormComponent {
    constructor(userService, route, router) {
        this.userService = userService;
        this.route = route;
        this.router = router;
        this.newPerson = new person_1.Person("", "");
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['index'] != "new") {
                this.index = params['index'];
                this.userService.list().subscribe((users) => {
                    this.newPerson = users[params['index']];
                });
            }
        });
    }
    saveName() {
        if (this.index) {
            this.userService.edit(this.newPerson, this.index).subscribe((users) => {
                this.router.navigate(['list']);
            });
        }
        else {
            this.userService.add(this.newPerson).subscribe((users) => {
                this.router.navigate(['list']);
            });
        }
        this.newPerson = new person_1.Person("", "");
    }
    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
};
FormComponent = __decorate([
    core_1.Component({
        selector: 'user-form',
        templateUrl: 'app11/form.component.html',
    }),
    __metadata("design:paramtypes", [user_service_1.UserService,
        router_1.ActivatedRoute,
        router_1.Router])
], FormComponent);
exports.FormComponent = FormComponent;
//# sourceMappingURL=form.component.js.map