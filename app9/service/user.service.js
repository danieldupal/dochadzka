"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Observable_1 = require("rxjs/Observable");
const person_1 = require("../model/person");
class UserService {
    constructor() {
        this.users = [
            new person_1.Person("Adam", "Krt"),
            new person_1.Person("Ondras", "Machula", "56316846519"),
            new person_1.Person("Jozef", "Mak", "1234567890")
        ];
    }
    list() {
        return new Observable_1.Observable((subscriber) => {
            subscriber.next(this.users);
        });
    }
    add(person) {
        return new Observable_1.Observable((subscriber) => {
            this.users.push(person);
            subscriber.next(this.users);
        });
    }
}
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map