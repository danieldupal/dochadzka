import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Person } from './model/person';
import { UserService } from './service/user.service';

@Component({
    selector: 'user-list',
    template: `
        <p *ngFor="let user of userList; let i = index" [attr.data-index]="i">
        {{user.firstName}} {{user.lastName}} {{user.isic ? user.isic : "-"}} 
        <button (click)="edit(i)">uprav</button>
        </p>
    `
})
export class ListComponent {
    
    userList:Array<Person>;

    constructor(private userService: UserService,
                private router: Router) {}

    ngOnInit() {
        this.userService.list().subscribe((users:Array<Person>) => {
            this.userList = users;
        });
    }

    edit(index:number):void {
        this.router.navigate(['user/'+index]);
    }
}
