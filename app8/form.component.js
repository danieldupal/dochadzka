"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@angular/core");
const person_1 = require("./model/person");
let FormComponent = class FormComponent {
    constructor() {
        this.userUpdated = new core_1.EventEmitter();
    }
    saveName() {
        this.userUpdated.emit(this.newPerson);
        this.newPerson = new person_1.Person("", "");
    }
};
__decorate([
    core_1.Output('updatePerson'),
    __metadata("design:type", Object)
], FormComponent.prototype, "userUpdated", void 0);
__decorate([
    core_1.Input('editPerson'),
    __metadata("design:type", person_1.Person)
], FormComponent.prototype, "newPerson", void 0);
FormComponent = __decorate([
    core_1.Component({
        selector: 'user-form',
        templateUrl: 'app8/form.component.html',
    }),
    __metadata("design:paramtypes", [])
], FormComponent);
exports.FormComponent = FormComponent;
//# sourceMappingURL=form.component.js.map